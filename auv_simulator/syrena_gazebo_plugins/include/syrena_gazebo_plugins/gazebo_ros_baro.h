//=================================================================================================
// Copyright (c) 2012, Johannes Meyer, TU Darmstadt
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Flight Systems and Automatic Control group,
//       TU Darmstadt, nor the names of its contributors may be used to
//       endorse or promote products derived from this software without
//       specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//=================================================================================================

#ifndef SYRENA_GAZEBO_PLUGINS_GAZEBO_ROS_BARO_H
#define SYRENA_GAZEBO_PLUGINS_GAZEBO_ROS_BARO_H

#include <gazebo/common/Plugin.hh>

#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <hector_gazebo_plugins/sensor_model.h>
#include <hector_gazebo_plugins/update_timer.h>

#include <dynamic_reconfigure/server.h>

namespace gazebo
{
   class GazeboRosBaro : public ModelPlugin
   {
   public:
      /// \brief Constructor
      GazeboRosBaro();

      /// \brief Destructor
      virtual ~GazeboRosBaro();

   protected:
      virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
      virtual void Reset();
      virtual void Update();

   private:
      /// \brief The parent World
      physics::WorldPtr world;

      /// \brief The link referred to by this plugin
      physics::LinkPtr link;

      /// \brief pointer to ros node
      ros::NodeHandle* node_handle_;
      ros::Publisher pub_;

      /// \brief ros message
      nav_msgs::Odometry odom;

      /// \brief store link name
      std::string link_name_;

      /// \brief frame id
      std::string frame_id_;

      /// \brief topic name
      std::string topic_;

      /// \brief allow specifying constant xyz and rpy offsets
      math::Pose offset_;

      /// \brief Sensor models
      SensorModel3 posModel;

      /// \brief A mutex to lock access to fields that are used in message callbacks
      boost::mutex lock;

      /// \brief save current body/physics state
      math::Vector3 pos;

      /// \brief for setting ROS name space
      std::string namespace_;

      UpdateTimer updateTimer;
      event::ConnectionPtr updateConnection;
   };
}

#endif // HECTOR_GAZEBO_PLUGINS_GAZEBO_ROS_Baro_H
