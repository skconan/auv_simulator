/*
 * Copyright 2013 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/*
   Desc: GazeboRosImagingSonar plugin for simulating cameras in Gazebo
   Author: John Hsu
   Date: 24 Sept 2008
 */

#include <algorithm>
#include <assert.h>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#include <syrena_gazebo_plugins/gazebo_ros_imaging_sonar.h>
// #include <gazebo_plugins/gazebo_ros_openni_kinect.h>

#include <gazebo/sensors/Sensor.hh>
#include <sdf/sdf.hh>
#include <gazebo/sensors/SensorTypes.hh>

#include <sensor_msgs/point_cloud2_iterator.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <pcl_ros/transforms.h>

#include <tf/tf.h>

namespace gazebo
{
// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(GazeboRosImagingSonar)

////////////////////////////////////////////////////////////////////////////////
// Constructor
GazeboRosImagingSonar::GazeboRosImagingSonar() //:pcl_cloud_ (new pcl::PointCloud<pcl::PointXYZRGB> ())
{
  this->point_cloud_connect_count_ = 0;
  this->depth_info_connect_count_ = 0;
  this->depth_image_connect_count_ = 0;
  this->last_depth_image_camera_info_update_time_ = common::Time(0);

  this->pcl_cloud_ = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
  // this->pcl_tree_ = pcl::search::KdTree<pcl::PointXYZRGB>::Ptr(new pcl::search::KdTree<pcl::PointXYZRGB>());
  this->pcl_cloud_normal_ = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
GazeboRosImagingSonar::~GazeboRosImagingSonar()
{
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void GazeboRosImagingSonar::Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
{
  DepthCameraPlugin::Load(_parent, _sdf);

  // copying from DepthCameraPlugin into GazeboRosCameraUtils
  this->parentSensor_ = this->parentSensor;
  this->width_ = this->width;
  this->height_ = this->height;
  this->depth_ = this->depth;
  this->format_ = this->format;
  this->camera_ = this->depthCamera;

  ROS_WARN("imaging_sonar_node start !!!!!!!!!");
  // using a different default
  if (!_sdf->HasElement("imageTopicName"))
    this->image_topic_name_ = "ir/image_raw";
  if (!_sdf->HasElement("cameraInfoTopicName"))
    this->camera_info_topic_name_ = "ir/camera_info";

  // point cloud stuff
  if (!_sdf->HasElement("pointCloudTopicName"))
    this->point_cloud_topic_name_ = "points";
  else
    this->point_cloud_topic_name_ = _sdf->GetElement("pointCloudTopicName")->Get<std::string>();

  // depth image stuff
  if (!_sdf->HasElement("depthImageTopicName"))
    this->depth_image_topic_name_ = "depth/image_raw";
  else
    this->depth_image_topic_name_ = _sdf->GetElement("depthImageTopicName")->Get<std::string>();

  if (!_sdf->HasElement("depthImageCameraInfoTopicName"))
    this->depth_image_camera_info_topic_name_ = "depth/camera_info";
  else
    this->depth_image_camera_info_topic_name_ = _sdf->GetElement("depthImageCameraInfoTopicName")->Get<std::string>();

  if (!_sdf->HasElement("pointCloudCutoff"))
    this->point_cloud_cutoff_ = 0.4;
  else
    this->point_cloud_cutoff_ = _sdf->GetElement("pointCloudCutoff")->Get<double>();
  if (!_sdf->HasElement("pointCloudCutoffMax"))
    this->point_cloud_cutoff_max_ = 5.0;
  else
    this->point_cloud_cutoff_max_ = _sdf->GetElement("pointCloudCutoffMax")->Get<double>();

  load_connection_ = GazeboRosCameraUtils::OnLoad(boost::bind(&GazeboRosImagingSonar::Advertise, this));
  GazeboRosCameraUtils::Load(_parent, _sdf);
}

void GazeboRosImagingSonar::Advertise()
{
  ros::AdvertiseOptions point_cloud_ao =
      ros::AdvertiseOptions::create<sensor_msgs::PointCloud2>(
          this->point_cloud_topic_name_, 1,
          boost::bind(&GazeboRosImagingSonar::PointCloudConnect, this),
          boost::bind(&GazeboRosImagingSonar::PointCloudDisconnect, this),
          ros::VoidPtr(), &this->camera_queue_);
  this->point_cloud_pub_ = this->rosnode_->advertise(point_cloud_ao);

  ros::AdvertiseOptions depth_image_ao =
      ros::AdvertiseOptions::create<sensor_msgs::Image>(
          this->depth_image_topic_name_, 1,
          boost::bind(&GazeboRosImagingSonar::DepthImageConnect, this),
          boost::bind(&GazeboRosImagingSonar::DepthImageDisconnect, this),
          ros::VoidPtr(), &this->camera_queue_);
  this->depth_image_pub_ = this->rosnode_->advertise(depth_image_ao);

  ros::AdvertiseOptions depth_image_camera_info_ao =
      ros::AdvertiseOptions::create<sensor_msgs::CameraInfo>(
          this->depth_image_camera_info_topic_name_, 1,
          boost::bind(&GazeboRosImagingSonar::DepthInfoConnect, this),
          boost::bind(&GazeboRosImagingSonar::DepthInfoDisconnect, this),
          ros::VoidPtr(), &this->camera_queue_);
  this->depth_image_camera_info_pub_ = this->rosnode_->advertise(depth_image_camera_info_ao);
}

////////////////////////////////////////////////////////////////////////////////
// Increment count
void GazeboRosImagingSonar::PointCloudConnect()
{
  this->point_cloud_connect_count_++;
  (*this->image_connect_count_)++;
  this->parentSensor->SetActive(true);
}
////////////////////////////////////////////////////////////////////////////////
// Decrement count
void GazeboRosImagingSonar::PointCloudDisconnect()
{
  this->point_cloud_connect_count_--;
  (*this->image_connect_count_)--;
  if (this->point_cloud_connect_count_ <= 0)
    this->parentSensor->SetActive(false);
}

////////////////////////////////////////////////////////////////////////////////
// Increment count
void GazeboRosImagingSonar::DepthImageConnect()
{
  this->depth_image_connect_count_++;
  this->parentSensor->SetActive(true);
}
////////////////////////////////////////////////////////////////////////////////
// Decrement count
void GazeboRosImagingSonar::DepthImageDisconnect()
{
  this->depth_image_connect_count_--;
}

////////////////////////////////////////////////////////////////////////////////
// Increment count
void GazeboRosImagingSonar::DepthInfoConnect()
{
  this->depth_info_connect_count_++;
}
////////////////////////////////////////////////////////////////////////////////
// Decrement count
void GazeboRosImagingSonar::DepthInfoDisconnect()
{
  this->depth_info_connect_count_--;
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void GazeboRosImagingSonar::OnNewDepthFrame(const float *_image,
                                            unsigned int _width, unsigned int _height, unsigned int _depth,
                                            const std::string &_format)
{
  if (!this->initialized_ || this->height_ <= 0 || this->width_ <= 0)
    return;

  this->depth_sensor_update_time_ = this->parentSensor->LastUpdateTime();
  if (this->parentSensor->IsActive())
  {
    if (this->point_cloud_connect_count_ <= 0 &&
        this->depth_image_connect_count_ <= 0 &&
        (*this->image_connect_count_) <= 0)
    {
      this->parentSensor->SetActive(false);
    }
    else
    {
      if (this->point_cloud_connect_count_ > 0)
        this->FillPointdCloud(_image);

      if (this->depth_image_connect_count_ > 0)
        this->FillDepthImage(_image);
    }
  }
  else
  {
    if (this->point_cloud_connect_count_ > 0 ||
        this->depth_image_connect_count_ <= 0)
      // do this first so there's chance for sensor to run 1 frame after activate
      this->parentSensor->SetActive(true);
  }
  PublishCameraInfo();
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void GazeboRosImagingSonar::OnNewImageFrame(const unsigned char *_image,
                                            unsigned int _width, unsigned int _height, unsigned int _depth,
                                            const std::string &_format)
{
  if (!this->initialized_ || this->height_ <= 0 || this->width_ <= 0)
    return;

  //ROS_ERROR("camera_ new frame %s %s",this->parentSensor_->Name().c_str(),this->frame_name_.c_str());
  this->sensor_update_time_ = this->parentSensor_->LastUpdateTime();

  if (this->parentSensor->IsActive())
  {
    if (this->point_cloud_connect_count_ <= 0 &&
        this->depth_image_connect_count_ <= 0 &&
        (*this->image_connect_count_) <= 0)
    {
      this->parentSensor->SetActive(false);
    }
    else
    {
      if ((*this->image_connect_count_) > 0)
        this->PutCameraData(_image);
    }
  }
  else
  {
    if ((*this->image_connect_count_) > 0)
      // do this first so there's chance for sensor to run 1 frame after activate
      this->parentSensor->SetActive(true);
  }
}

////////////////////////////////////////////////////////////////////////////////
// Put point cloud data to the interface
void GazeboRosImagingSonar::FillPointdCloud(const float *_src)
{
  this->lock_.lock();

  this->point_cloud_msg_.header.frame_id = this->frame_name_;
  this->point_cloud_msg_.header.stamp.sec = this->depth_sensor_update_time_.sec;
  this->point_cloud_msg_.header.stamp.nsec = this->depth_sensor_update_time_.nsec;
  ///copy from depth to point cloud message
  FillPointCloudHelper(this->point_cloud_msg_,
                       this->height,
                       this->width,
                       this->skip_,
                       (void *)_src);

  // pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::Normal> normal_estimator;
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr pcl_tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
  // pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

  // ne.setRadiusSearch(0.5);

  // ROS_INFO("%d",(int)this->pcl_cloud_->points.size());

  this->normal_estimator_.setSearchMethod(pcl_tree);
  this->normal_estimator_.setKSearch(10);
  
  this->normal_estimator_.setInputCloud(this->pcl_cloud_);
  this->normal_estimator_.compute(*this->pcl_cloud_normal_);

  static pcl::PointCloud<pcl::PointXYZRGB>::iterator p;
  static pcl::PointCloud<pcl::Normal>::iterator n;

  p = this->pcl_cloud_->points.begin();
  n = this->pcl_cloud_normal_->points.begin();

  // ROS_INFO("%d == %d",cloud_normals->points.size(),this->pcl_cloud_->points.size());

  sensor_msgs::PointCloud2Modifier pcd_modifier(this->point_cloud_msg_);
  pcd_modifier.setPointCloud2Fields(7, "x", 1, sensor_msgs::PointField::FLOAT32,
                                    "y", 1, sensor_msgs::PointField::FLOAT32,
                                    "z", 1, sensor_msgs::PointField::FLOAT32,
                                    "rgb", 1, sensor_msgs::PointField::FLOAT32,
                                    "r", 1, sensor_msgs::PointField::FLOAT32,
                                    "theta", 1, sensor_msgs::PointField::FLOAT32,
                                    "ra", 1, sensor_msgs::PointField::FLOAT32); // reflection angle

  pcd_modifier.resize(this->width * this->height);

  sensor_msgs::PointCloud2Iterator<float> it_x(this->point_cloud_msg_, "x");
  sensor_msgs::PointCloud2Iterator<float> it_y(this->point_cloud_msg_, "y");
  sensor_msgs::PointCloud2Iterator<float> it_z(this->point_cloud_msg_, "z");
  sensor_msgs::PointCloud2Iterator<float> it_rgb(this->point_cloud_msg_, "rgb");
  sensor_msgs::PointCloud2Iterator<float> it_r(this->point_cloud_msg_, "r");
  sensor_msgs::PointCloud2Iterator<float> it_theta(this->point_cloud_msg_, "theta");
  sensor_msgs::PointCloud2Iterator<float> it_ra(this->point_cloud_msg_, "ra"); // reflection angle

  for (; p < this->pcl_cloud_->points.end();)
  {

    *it_x = p->x;
    *it_y = p->y;
    *it_z = p->z;

    *it_rgb = p->rgb;

    *it_r = sqrt((p->x) * (p->x) + (p->y) * (p->y) + (p->z) * (p->z));
    *it_theta = atan2(p->x, p->z);

    float reflection_angle = ((n->normal_x*p->x)+(n->normal_y*p->y)+(n->normal_z*p->z))/ *it_r;
    *it_ra = fabs(reflection_angle);
    //fabs(n->normal_z);

    n++; // normal
    p++;
    ++it_x;
    ++it_y;
    ++it_z;
    ++it_rgb;
    ++it_r;
    ++it_theta;
    ++it_ra;
  }

  this->point_cloud_msg_.width = this->width;
  this->point_cloud_msg_.height = this->height;
  this->point_cloud_msg_.row_step = this->point_cloud_msg_.point_step * this->width;

  this->point_cloud_msg_.is_dense = this->pcl_cloud_->is_dense;

  // pcl::toROSMsg(*this->pcl_cloud_, this->ros_cloud_);
  this->point_cloud_pub_.publish(this->point_cloud_msg_);

  this->lock_.unlock();
}

////////////////////////////////////////////////////////////////////////////////
// Put depth image data to the interface
void GazeboRosImagingSonar::FillDepthImage(const float *_src)
{
  this->lock_.lock();
  // copy data into image
  this->depth_image_msg_.header.frame_id = this->frame_name_;
  this->depth_image_msg_.header.stamp.sec = this->depth_sensor_update_time_.sec;
  this->depth_image_msg_.header.stamp.nsec = this->depth_sensor_update_time_.nsec;

  ///copy from depth to depth image message
  FillDepthImageHelper(this->depth_image_msg_,
                       this->height,
                       this->width,
                       this->skip_,
                       (void *)_src);

  this->depth_image_pub_.publish(this->depth_image_msg_);

  this->lock_.unlock();
}

// Fill depth information
bool GazeboRosImagingSonar::FillPointCloudHelper(
    sensor_msgs::PointCloud2 &point_cloud_msg,
    uint32_t rows_arg, uint32_t cols_arg,
    uint32_t step_arg, void *data_arg)
{
  this->pcl_cloud_->width = cols_arg;
  this->pcl_cloud_->height = rows_arg;
  this->pcl_cloud_->is_dense = true;

  this->pcl_cloud_->points.resize(cols_arg * rows_arg);

  float *toCopyFrom = (float *)data_arg;
  int index = 0;

  double hfov = this->parentSensor->DepthCamera()->HFOV().Radian();
  double fl = ((double)this->width) / (2.0 * tan(hfov / 2.0));

  pcl::PointCloud<pcl::PointXYZRGB>::iterator p;
  p = this->pcl_cloud_->points.begin();

  // convert depth to point cloud
  for (uint32_t j = 0; j < rows_arg; j++)
  {
    double pAngle;
    if (rows_arg > 1)
      pAngle = atan2((double)j - 0.5 * (double)(rows_arg - 1), fl);
    else
      pAngle = 0.0;

    for (uint32_t i = 0; i < cols_arg; i++, p++)
    {
      double yAngle;
      if (cols_arg > 1)
        yAngle = atan2((double)i - 0.5 * (double)(cols_arg - 1), fl);
      else
        yAngle = 0.0;

      double depth = toCopyFrom[index++]; // + 0.0*this->myParent->GetNearClip();

      if (depth > this->point_cloud_cutoff_ &&
          depth < this->point_cloud_cutoff_max_)
      {
        p->x = depth * tan(yAngle);
        p->y = depth * tan(pAngle);
        p->z = depth;
      }
      else //point in the unseeable range
      {
        p->x = p->y = p->z = std::numeric_limits<float>::quiet_NaN();

        this->pcl_cloud_->is_dense = false;
      }

      // put image color data for each point
      uint8_t *image_src = (uint8_t *)(&(this->image_msg_.data[0]));
      if (this->image_msg_.data.size() == rows_arg * cols_arg * 3)
      {
        // color
        p->r = image_src[i * 3 + j * cols_arg * 3 + 0];
        p->g = image_src[i * 3 + j * cols_arg * 3 + 1];
        p->b = image_src[i * 3 + j * cols_arg * 3 + 2];
      }
      else
      {
        p->r = 0;
        p->g = 0;
        p->b = 0;
      }
    }
  }

  return true;
}

// Fill depth information
bool GazeboRosImagingSonar::FillDepthImageHelper(
    sensor_msgs::Image &image_msg,
    uint32_t rows_arg, uint32_t cols_arg,
    uint32_t step_arg, void *data_arg)
{
  image_msg.encoding = sensor_msgs::image_encodings::TYPE_32FC1;
  image_msg.height = rows_arg;
  image_msg.width = cols_arg;
  image_msg.step = sizeof(float) * cols_arg;
  image_msg.data.resize(rows_arg * cols_arg * sizeof(float));
  image_msg.is_bigendian = 0;

  const float bad_point = std::numeric_limits<float>::quiet_NaN();

  float *dest = (float *)(&(image_msg.data[0]));
  float *toCopyFrom = (float *)data_arg;
  int index = 0;

  // convert depth to point cloud
  for (uint32_t j = 0; j < rows_arg; j++)
  {
    for (uint32_t i = 0; i < cols_arg; i++)
    {
      float depth = toCopyFrom[index++];

      if (depth > this->point_cloud_cutoff_ &&
          depth < this->point_cloud_cutoff_max_)
      {
        dest[i + j * cols_arg] = depth;
      }
      else //point in the unseeable range
      {
        dest[i + j * cols_arg] = bad_point;
      }
    }
  }
  return true;
}

void GazeboRosImagingSonar::PublishCameraInfo()
{
  ROS_DEBUG("publishing default camera info, then openni kinect camera info");
  GazeboRosCameraUtils::PublishCameraInfo();

  if (this->depth_info_connect_count_ > 0)
  {
    this->sensor_update_time_ = this->parentSensor_->LastUpdateTime();
    common::Time cur_time = this->world_->GetSimTime();
    if (cur_time - this->last_depth_image_camera_info_update_time_ >= this->update_period_)
    {
      this->PublishCameraInfo(this->depth_image_camera_info_pub_);
      this->last_depth_image_camera_info_update_time_ = cur_time;
    }
  }
}

//@todo: publish disparity similar to openni_camera_deprecated/src/nodelets/openni_nodelet.cpp.
/*
#include <stereo_msgs/DisparityImage.h>
pub_disparity_ = comm_nh.advertise<stereo_msgs::DisparityImage > ("depth/disparity", 5, subscriberChanged2, subscriberChanged2);

void GazeboRosDepthCamera::PublishDisparityImage(const DepthImage& depth, ros::Time time)
{
  stereo_msgs::DisparityImagePtr disp_msg = boost::make_shared<stereo_msgs::DisparityImage > ();
  disp_msg->header.stamp                  = time;
  disp_msg->header.frame_id               = device_->isDepthRegistered () ? rgb_frame_id_ : depth_frame_id_;
  disp_msg->image.header                  = disp_msg->header;
  disp_msg->image.encoding                = sensor_msgs::image_encodings::TYPE_32FC1;
  disp_msg->image.height                  = depth_height_;
  disp_msg->image.width                   = depth_width_;
  disp_msg->image.step                    = disp_msg->image.width * sizeof (float);
  disp_msg->image.data.resize (disp_msg->image.height * disp_msg->image.step);
  disp_msg->T = depth.getBaseline ();
  disp_msg->f = depth.getFocalLength () * depth_width_ / depth.getWidth ();

  /// @todo Compute these values from DepthGenerator::GetDeviceMaxDepth() and the like
  disp_msg->min_disparity = 0.0;
  disp_msg->max_disparity = disp_msg->T * disp_msg->f / 0.3;
  disp_msg->delta_d = 0.125;

  depth.fillDisparityImage (depth_width_, depth_height_, reinterpret_cast<float*>(&disp_msg->image.data[0]), disp_msg->image.step);

  pub_disparity_.publish (disp_msg);
}
*/
}
