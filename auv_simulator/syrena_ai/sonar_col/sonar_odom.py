import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


DEG_PER_PIXEL = 0.3217821782178218  # 130/404.0
RAD_PER_PIXEL = np.radians(DEG_PER_PIXEL)

xy = pd.read_csv('xy.csv', sep=' ', header=None)
polar = pd.read_csv('polar.csv', sep=' ', header=None)


dx, dy, xy_co = xy.values.T
r, theta, polar_co = polar.values.T

THETA = 0


position = (0, 0)
heading = 0


def move(position, heading, dx, dy, dtheta):

    x, y = position

    heading += dtheta

    heading = heading % 360

    rad_heading = np.radians(heading)

    x += dx * np.cos(rad_heading) - dy * np.sin(rad_heading)
    y += dx * np.sin(rad_heading) + dy * np.cos(rad_heading)

    return (x, y), heading

data_x = []
data_y = []

FACTOR_ROTATION = DEG_PER_PIXEL * 360 / 170.0 


FACTOR_XY = 25/150.0

for i in xrange(r.size):

    d_theta = theta[i] * FACTOR_ROTATION

    d_x = dx[i] * FACTOR_XY
    d_y = dy[i] * FACTOR_XY

    position, heading = move(position, heading, d_x, d_y, d_theta)
    # THETA += d_theta

    # print

    data_x.append(position[0])
    data_y.append(position[1])

    # data_y.append(THETA)


plt.scatter(data_x, data_y)

plt.grid(True)

plt.axes().set_aspect('equal', 'datalim')

plt.show()


# print polar.values.shape
# print xy.values.shape
