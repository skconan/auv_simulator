import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys

filename = sys.argv[1]

display = True
if len(sys.argv) > 2:
    display = False

capture = cv2.VideoCapture(filename)


if "xy" in filename:
    pts = np.array([[254, 326], [18, 227], [254, 84],
                    [478, 227]], dtype=np.int32)
elif "polar" in filename:
    # pts = np.array([[250,63],[450,326],[450,326],[55,326]], dtype=np.int32)
    pts = np.array([[61, 63], [447, 63], [450, 326],
                    [55, 326]], dtype=np.int32)

# pts = np.array([[254,326],[18,227],[257,84],[478,227]], dtype=np.int32)
# pts = np.array([[55,56],[55,369],[450,369],[450,56]], dtype=np.int32)
# pts = np.array([[0, 0], [0, 396], [497, 396], [497, 0]], dtype=np.int32)

# pts = np.array([[170,219], [28,128], [199,36], [279,113]], dtype=np.int32)
# capture

DEG_PER_PIXEL = 0.3217821782178218  # 130/404.0
RAD_PER_PIXEL = np.radians(DEG_PER_PIXEL)

last = None

x, y = 0, 0

data_x = []
data_y = []

limit = 100#None
start = None#270

i = 0

FRAME_RATE = capture.get(cv2.CAP_PROP_FPS)

while True:

    ret, img = capture.read()

    i += 1
    if limit and start and i< start: continue

    if limit and start and i >= limit+start:
        break

    if ret is False:
        break

    mask = np.zeros((img.shape[0], img.shape[1]))

    cv2.fillConvexPoly(mask, pts, 1)
    mask = mask.astype(np.bool)
    out = np.zeros_like(img)
    out[mask] = img[mask]

    print img[mask].max()
    # np.savetxt('hist_avi.out', img[mask].ravel(),fmt='%d') ; exit()
    

    if display == True:
        cv2.imshow('some', out)

    current = np.float32(img[mask])

    if last is not None:
        d = cv2.phaseCorrelate(last, current)

        # if d[-1] < 0.8: print d[-1]
        dx, dy = d[0]

        if abs(dy) >= 100:
            print "><"*50,dy

            dy = 0

        print i,dx, dy, d[-1]
        # print "%7.4f %7.4f %7.4f"%(dx,dy,d[-1])#*0.3217821782178218
        x += dx
        y += dy# * DEG_PER_PIXEL  # *FRAME_RATE

        data_x += [x]
        data_y += [y]

    last = np.float32(current)
    # input()

    if 0xFF & cv2.waitKey(1) == 27:
        break

cv2.destroyAllWindows()

plt.scatter(data_y, range(len(data_y)))

plt.grid(True)

plt.axes().set_aspect('equal', 'datalim')

plt.show()
