#!/usr/bin/env python


import rospy

import numpy as np

# from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan, PointCloud2
import sensor_msgs.point_cloud2 as pc2

#from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud


class SonarToCloud(object):
    def __init__(self, arg=dict()):
        self.arg = arg

        try:
            rospy.init_node("sonar_to_cloud")
        except:
            pass

        rospy.Subscriber("/imaging_sonar/depth/points",
                         PointCloud2, self.cloud_update, queue_size=1)

        self.sonar_points_pub = rospy.Publisher("/imaging_sonar/sonar/points",PointCloud2,queue_size=10)

    def cloud_update(self, cloud):
        self.points = pc2.read_points(
            cloud, skip_nans=True, field_names=("x", "y", "z", "rgb"))

        x, y, z, rgb = np.array(list(self.points))[::3].T

        z = np.sqrt(y * y + z * z)
        y = np.zeros(y.shape)

        print y.shape
        sonar_points = np.c_[x,y,z]

        sonar_cloud = pc2.create_cloud(cloud.header,cloud.fields[:3],sonar_points)
        self.sonar_points_pub.publish(sonar_cloud)

    def run(self):

        msg = rospy.wait_for_message(
            "/imaging_sonar/depth/points", PointCloud2)

        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
        rospy.spin()


if __name__ == '__main__':

    x = SonarToCloud()
    x.run()
