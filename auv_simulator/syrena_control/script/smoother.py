#!/usr/bin/env python

import rospy

from geometry_msgs.msg import Twist
from support import *


def diff(t1, t2):
    return map(lambda x, y: y - x, t1, t2)


def plus(t1, t2):
    return map(lambda x, y: x + y, t1, t2)


def mul(t, k):
    return map(lambda x: x * k, t)


class Smoothy:
    A_LIMIT = (3, 5, 5, 3, 3, 3)
    HZ = 20.0

    def __init__(self):

        rospy.init_node('Smoothy')
        rospy.Subscriber("/cmd_vel", Twist, self.Callback)

        self.pub = rospy.Publisher('/cmd_vel/smoothed', Twist, queue_size=10)

        self.last_v = [0, 0, 0, 0, 0, 0]

        self.timeout = 0.2
        self.interval_time = 0.1

        self.period = 1.0 / self.HZ

        self.max_v_inc_scale = tuple(self.period * i for i in self.A_LIMIT)

        self.v_last = [0 for i in xrange(6)]
        self.v_target = [0 for i in xrange(6)]

        rate = rospy.Rate(self.HZ)

        while not rospy.is_shutdown():

            v_inc = diff(self.v_last, self.v_target)

            min_v_inc = map(lambda x, y: min(abs(x), abs(y)) *
                            (-1 if y < 0 else 1), self.max_v_inc_scale, v_inc)

            # print self.v_last
            output = plus(min_v_inc, self.v_last)

            pprint(output)

            self.pub.publish(ltt(output))

            self.v_last = list(output)
            rate.sleep()

        rospy.spin()

    def Callback(self, twist):  # joy
        self.v_target = ttl(twist)


if __name__ == '__main__':

    s = Smoothy()

    # ser.close()  # Only executes once the loop exits
