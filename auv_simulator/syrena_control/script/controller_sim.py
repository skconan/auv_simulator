#!/usr/bin/env python

import rospy
from controller import *

if __name__=='__main__':

    file_name = rospkg.RosPack().get_path('syrena_control')+"/param/PID_sim.yaml"
    X=Controller(file_name=file_name)


    z = float(rospy.get_param("/PID/init_z"))
    Y = float(rospy.get_param("/PID/init_Y"))

    X.isFixPoint = [0,0,1,1,1,0]
    X.lastPoint= [0,0,z,0,0,Y]
    #x.lastPoint = [3,2,-1,0,0,0]


    X.run()
    rospy.spin()



#roslaunch syrena_gazebo trandesc.launch
#rosrun syrena_control controller_sim.py
#rosrun rqt_publisher rqt_publisher
